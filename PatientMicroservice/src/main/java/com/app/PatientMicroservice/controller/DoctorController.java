package com.app.PatientMicroservice.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.PatientMicroservice.model.Doctor;
import com.app.PatientMicroservice.repository.DoctorRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class DoctorController
{

	@Autowired
	DoctorRepository doctorRepository;

	@ApiOperation(value = "Doctor List", notes = "This method will retreive list from database", nickname = "getDoctor")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),@ApiResponse(code = 404, message = "Doctor not found"),
    @ApiResponse(code = 200, message = "Successful retrieval", response = Doctor.class, responseContainer = "List") })

	//Passing parameters from service URL to repository
	@RequestMapping("/GetByDoctorId/{doctorId}")
	public List<Doctor> getBookByName(@PathVariable("doctorId") int doctorId) 
	{
		List<Doctor> dlist = doctorRepository.findByDoctorId(doctorId);
		return dlist;
	}
}