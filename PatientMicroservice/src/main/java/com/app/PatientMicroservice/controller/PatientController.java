package com.app.PatientMicroservice.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.PatientMicroservice.model.Patient;
import com.app.PatientMicroservice.repository.PatientRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class PatientController 
{

	@Autowired
	PatientRepository patientRepository;

	@ApiOperation(value = "Patient List", notes = "This method will retreive list from database", nickname = "getPatients")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),@ApiResponse(code = 404, message = "Patients not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = Patient.class, responseContainer = "List") })

	//Passing parameters from service URL to repository
	@RequestMapping("/Doctor/{patientId}/{diagnosisId}/{doctorId}")
	public List<Patient> getDoctorFunction(@PathVariable("patientId") int patientId,
			@PathVariable("diagnosisId") int diagnosisId, @PathVariable("doctorId") int doctorId)
	{
		List<Patient> dlist = patientRepository.getDoctorFunction(patientId, diagnosisId, doctorId);
		return dlist;
	}

	//Passing PatientId from Service URL
	@RequestMapping("/Patient/{patientId}")
	public List<Patient> getById(@PathVariable("patientId") int patientId) 
	{
		List<Patient> dlist = patientRepository.findByPatientId(patientId);
		return dlist;
	}
}
