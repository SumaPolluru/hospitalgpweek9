package com.app.PatientMicroservice.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.PatientMicroservice.model.Diagnosis;
import com.app.PatientMicroservice.repository.DiagnosisRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class DiagnosisController 
{

	@Autowired
	DiagnosisRepository diagnosisRepository;

	//Swagger
	@ApiOperation(value = "Diagnosis List", notes = "This method will retreive list from database", nickname = "getDiagnosis")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),@ApiResponse(code = 404, message = "Diagnosis not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = Diagnosis.class, responseContainer = "List") })

	//Passing parameters from service URL to repository
	@RequestMapping("/Diagnosis/{diagnosisId}")
	public List<Diagnosis> getBookByName(@PathVariable("diagnosisId") int diagnosisId) 
	{
		List<Diagnosis> dlist = diagnosisRepository.findByDiagnosisId(diagnosisId);
		return dlist;
	}
}
