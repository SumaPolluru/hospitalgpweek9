package com.app.PatientMicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableEurekaClient
//EurekaClient
public class PatientMicroserviceApplication 
{

	public static void main(String[] args) 
	{
		SpringApplication.run(PatientMicroserviceApplication.class, args);
	}

	//Swagger
	@Bean
	public Docket PatientApi() 
	{
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.app.PatientMicroservice")).build();
	}

	//Rest Template
	@Bean
	public RestTemplate getTemplate() 
	{
		return new RestTemplate();
	}

}
