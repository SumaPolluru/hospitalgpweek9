package com.app.PatientMicroservice.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Diagnosis")
//Table Creation
public class Diagnosis 
{

	@Id
	private int diagnosisId;
	private String diseaseName;

	public int getDiagnosisId()
	{
		return diagnosisId;
	}

	public void setDiagnosisId(int diagnosisId)
	{
		this.diagnosisId = diagnosisId;
	}

	public String getDiseaseName()
	{
		return diseaseName;
	}

	public void setDiseaseName(String diseaseName)
	{
		this.diseaseName = diseaseName;
	}
	
	//Relational Entity mapping one-many
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="diagnosisId")
	private List<Patient> plist;

	public List<Patient> getPlist() {
		return plist;
	}

	public void setPlist(List<Patient> plist) {
		this.plist = plist;
	}
	
}
