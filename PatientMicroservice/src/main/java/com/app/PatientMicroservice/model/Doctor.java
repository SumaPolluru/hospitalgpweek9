package com.app.PatientMicroservice.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Doctor")
//Table Creation
public class Doctor 
{

	@Id
	private int doctorId;
	private String doctorName;
	private String specialization;

	public int getDoctorId() 
	{
		return doctorId;
	}

	public void setDoctorId(int doctorId) 
	{
		this.doctorId = doctorId;
	}

	public String getDoctorName()
	{
		return doctorName;
	}

	public void setDoctorName(String doctorName) 
	{
		this.doctorName = doctorName;
	}

	public String getSpecialization()
	{
		return specialization;
	}

	public void setSpecialization(String specialization) 
	{
		this.specialization = specialization;
	}

	@Override
	public String toString() 
	{
		return "Doctor [doctorId=" + doctorId + ", doctorName=" + doctorName + ", specialization=" + specialization +"]";
	}
	
	//Relational Entity mapping one-many
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="doctorId")
	private List<Patient> plist;

	public List<Patient> getPlist() 
	{
		return plist;
	}

	public void setPlist(List<Patient> plist) 
	{
		this.plist = plist;
	}
	

}
