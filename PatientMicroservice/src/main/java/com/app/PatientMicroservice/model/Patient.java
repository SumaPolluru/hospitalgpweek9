package com.app.PatientMicroservice.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Patient")
//Table Creation
public class Patient
{

	@Id
	private int patientId;
	private String patientName;
	private String patientEmail;
	private int doctorId;
	private int diagnosisId;

	public int getDoctorId() 
	{
		return doctorId;
	}

	public void setDoctorId(int doctorId)
	{
		this.doctorId = doctorId;
	}

	public int getDiagnosisId() 
	{
		return diagnosisId;
	}

	public void setDiagnosisId(int diagnosisId)
	{
		this.diagnosisId = diagnosisId;
	}

	public int getPatientId() 
	{
		return patientId;
	}

	public void setPatientId(int patientId) 
	{
		this.patientId = patientId;
	}

	public String getPatientName()
	{
		return patientName;
	}

	public void setPatientName(String patientName)
	{
		this.patientName = patientName;
	}

	public String getPatientEmail() 
	{
		return patientEmail;
	}

	public void setPatientEmail(String patientEmail) 
	{
		this.patientEmail = patientEmail;
	}

}
