package com.app.PatientMicroservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.PatientMicroservice.model.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer>
{
	//Queries
	@Query("Select patient from Patient patient where patient.patientId=?1 and patient.diagnosisId=?2 and patient.doctorId=?3")
	public List<Patient> getDoctorFunction(int patientId,int diagnosisId,int doctorId);
	
	public List<Patient> findByPatientId(int patientId);
}
