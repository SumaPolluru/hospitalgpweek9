package com.app.PatientMicroservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.app.PatientMicroservice.model.Diagnosis;

@Repository
public interface DiagnosisRepository extends JpaRepository<Diagnosis, Integer>
{
	public List<Diagnosis> findByDiagnosisId(int diagnosisId);
}