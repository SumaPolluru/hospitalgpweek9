package com.app.PatientMicroservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.app.PatientMicroservice.model.Doctor;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer> 
{
	public List<Doctor> findByDoctorId(int doctorId);
	
}
