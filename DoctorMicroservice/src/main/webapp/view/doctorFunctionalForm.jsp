<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">
</head>
<body>

	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/home" class="navbar-brand"><font color=white size=4px>Doctor-Page</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/getByPatientId"><font color=white size=4px>Get
								By PatientID</font></a></li>
					<li><a href="/doctorTest"><font color=white size=4px>Doctor
								Functionality Test</font></a></li>
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${patientobj=='PATIENT_HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h2>
						<font size=8px>Welcome to Patient MicroService</font>
					</h2>
					<h3>Patient Operations</h3>
				</div>
			</div>
		</c:when>

		<c:when test="${obj=='PATIENT_DATA' }">
			<div class="container text-center">
				<h3>Enter Details For Functional Testing</h3>
				<hr>
				<form class="form-horizontal" action="/patient">
					<div class="form-group">
						<label class="control-label col-md-3">Patient Id</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="patientId"
								value="${patient.patientId}" placeholder="Patient Id" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Diagnosis Id</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="diagnosisId"
								value="${patient.diagnosisId}" placeholder="Diagnosis Id"
								required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Doctor Id</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="doctorId"
								value="${patient.doctorId}" placeholder="Doctor Id" required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Submit" />
					</div>
				</form>
			</div>
		</c:when>


		<c:when test="${obj=='PATIENT_ID' }">
			<div class="container text-center">
				<h3>Enter Patient Id</h3>
				<hr>
				<form class="form-horizontal" action="/patientBYId">
					<div class="form-group">
						<label class="control-label col-md-3">Patient Id</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="patientId"
								value="${patient.patientId}" placeholder="Patient Id" required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Check" />
					</div>
				</form>
			</div>
		</c:when>

	</c:choose>
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>