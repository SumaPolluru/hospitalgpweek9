<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<title>Doctor MicroService</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">

</head>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<div class="navbar-collapse collapse">
				<div class="page-header">
					<center>
						<h1 style="color: white;">MicroService Project</h1>
					</center>
				</div>
			</div>
		</div>
	</div>

	<div class="has-bg-img">
		<img class="bg-img" src="assets/img/slide/BackgroundImage.jpg"
			alt="Not Found">
		<div class="carousel-container">
			<center>
				<div class="carousel-content">
					<h2 class="animate__animated animate__fadeInDown">Doctor
						Functionality</h2>
					<p class="animate__animated animate__fadeInUp">Doctor</p>
					<h1>
						<a href="/data"
							class="btn-get-started animate__animated animate__fadeInUp scrollto">Doctor</a>
					</h1>
					<div class="carousel-content">
						<h2 class="animate__animated animate__fadeInDown">Patient
							Functionality</h2>
						<p class="animate__animated animate__fadeInUp">Patient
							Consumer</p>
						<h1>
							<a href="/home"
								class="btn-get-started animate__animated animate__fadeInUp scrollto">Patient</a>
						</h1>
					</div>
			</center>
		</div>
	</div>
	
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>