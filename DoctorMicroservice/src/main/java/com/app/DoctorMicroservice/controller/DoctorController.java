package com.app.DoctorMicroservice.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.app.DoctorMicroservice.model.Doctor;
import com.app.DoctorMicroservice.repository.DoctorRepository;

@RestController
public class DoctorController 
{
	
	@Autowired
	@Qualifier("b1")
	DoctorRepository doctorRepository;
    
	//Home
	@RequestMapping("/data")
	public ModelAndView Welcome(HttpServletRequest request)
	{
		request.setAttribute("doctorobj", "DOCTOR_HOME");
		return new ModelAndView("doctorForm");
	}
	
	//Get By Id form
	@RequestMapping("/getByDoctorId")
	public ModelAndView byIdTest(HttpServletRequest request)
	{
		request.setAttribute("obj","DOCTOR_ID"); 
		return new ModelAndView("doctorForm");
	}
	
	//Retrieving the data based on given ID
	@GetMapping("/doctorBYId")
	public List<Doctor> getById(@RequestParam int doctorId)
	{
		List<Doctor> pdetails=doctorRepository.findByDoctorId(doctorId);
		return pdetails;
				
	}
	
    //Logout
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request) 
	{
		 HttpSession httpSession = request.getSession();
		 httpSession.invalidate();
		 return new ModelAndView("redirect:/");
	}

}
