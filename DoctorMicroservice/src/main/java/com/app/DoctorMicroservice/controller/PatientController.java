package com.app.DoctorMicroservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.app.DoctorMicroservice.model.Patient;
import com.app.DoctorMicroservice.repository.PatientRepository;

@RestController
public class PatientController 
{
	
	@Autowired
	@Qualifier("b2")
	PatientRepository patientRepository;
    
	//Webpage starts
	@RequestMapping("/")
	public ModelAndView welcomepage()
	{
		System.out.println("Coming here ");
		return new ModelAndView("welcomePage");
	}
	
	@RequestMapping("/home")
	public ModelAndView Welcome(HttpServletRequest request)
	{
		request.setAttribute("patientobj", "PATIENT_HOME");
		return new ModelAndView("doctorFunctionalForm");
	}
	
	//Get by PatientId
	@RequestMapping("/getByPatientId")
	public ModelAndView byIdTest(HttpServletRequest request)
	{
		request.setAttribute("obj","PATIENT_ID"); 
		return new ModelAndView("doctorFunctionalForm");
	}
	
	//Retrieve Details based on given patientId
	@GetMapping("/patientBYId")
	public List<Patient> getById(@RequestParam int patientId)
	{
		List<Patient> pdetails=patientRepository.findByPatientId(patientId);
		return pdetails;
				
	}
	
	//Testing the doctor Functionality based on three parameters
	@RequestMapping("/doctorTest")
	public ModelAndView functionalTest(HttpServletRequest request)
	{
		request.setAttribute("obj","PATIENT_DATA"); 
		return new ModelAndView("doctorFunctionalForm");
	}
	
	//Retrieving the Details
	@GetMapping("/patient")
	public List<Patient> getByName(@RequestParam int patientId,@RequestParam int diagnosisId,@RequestParam int doctorId)
	{
		List<Patient> pdetails=patientRepository.getDoctorFunction(patientId,diagnosisId,doctorId);
		return pdetails;
				
	}

}
