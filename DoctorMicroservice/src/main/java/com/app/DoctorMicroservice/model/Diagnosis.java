package com.app.DoctorMicroservice.model;

//Diagnosis class for storing the data of diagnosis table
public class Diagnosis 
{

	private int diagnosisId;
	private String diseaseName;

	public int getDiagnosisId() 
	{
		return diagnosisId;
	}

	public void setDiagnosisId(int diagnosisId) 
	{
		this.diagnosisId = diagnosisId;
	}

	public String getDiseaseName() 
	{
		return diseaseName;
	}

	public void setDiseaseName(String diseaseName) 
	{
		this.diseaseName = diseaseName;
	}

}
