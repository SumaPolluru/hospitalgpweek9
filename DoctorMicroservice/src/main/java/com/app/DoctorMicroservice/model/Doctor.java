package com.app.DoctorMicroservice.model;

//Doctor class for storing the data of doctor table
public class Doctor 
{
	private int doctorId;
	private String doctorName;
	private String specialization;

	public int getDoctorId() 
	{
		return doctorId;
	}

	public void setDoctorId(int doctorId) 
	{
		this.doctorId = doctorId;
	}

	public String getDoctorName() 
	{
		return doctorName;
	}

	public void setDoctorName(String doctorName)
	{
		this.doctorName = doctorName;
	}

	public String getSpecialization() 
	{
		return specialization;
	}

	public void setSpecialization(String specialization) 
	{
		this.specialization = specialization;
	}
}
