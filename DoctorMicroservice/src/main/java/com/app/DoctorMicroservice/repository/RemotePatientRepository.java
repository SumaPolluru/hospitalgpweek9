package com.app.DoctorMicroservice.repository;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.app.DoctorMicroservice.model.Patient;

@Component
public class RemotePatientRepository implements PatientRepository {

	@Autowired
	protected RestTemplate restTemplate;

	protected String serviceUrl;
	
	//Default Constructor
	public RemotePatientRepository() 
	{

	}

	//Parameterized Constructor
	public RemotePatientRepository(String serviceUrl)
	{
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
	}

	//Method to retrieve based some parameters , passing to service URL
	@Override
	public List<Patient> getDoctorFunction(int patientId, int diagnosisId, int doctorId) 
	{
		System.out.println("Service url is " + serviceUrl);
		Patient[] doctorDetails = restTemplate.getForObject(serviceUrl + "/Doctor/{patientId}/{diagnosisId}/{doctorId}",
				Patient[].class, patientId, diagnosisId, doctorId);
		return Arrays.asList(doctorDetails);
	}

	//Method to retrieve based on PatientId, passing to service URL
	@Override
	public List<Patient> findByPatientId(int patientId) 
	{
		System.out.println("Service url is " + serviceUrl);
		Patient[] pdetails = restTemplate.getForObject(serviceUrl + "/Patient/{patientId}", Patient[].class, patientId);
		return Arrays.asList(pdetails);
	}
}