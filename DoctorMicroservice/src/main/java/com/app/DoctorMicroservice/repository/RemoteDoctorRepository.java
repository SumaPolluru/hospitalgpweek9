package com.app.DoctorMicroservice.repository;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.app.DoctorMicroservice.model.Doctor;

@Component
public class RemoteDoctorRepository implements DoctorRepository 
{

	@Autowired
	protected RestTemplate restTemplate;

	protected String serviceUrl;

	//Default Constructor
	public RemoteDoctorRepository() 
	{

	}

	//Parameterized Constructor
	public RemoteDoctorRepository(String serviceUrl) 
	{
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
	}

	//Method to retrieve based on DoctorId, passing to service URL
	@Override
	public List<Doctor> findByDoctorId(int doctorId) 
	{
		System.out.println("Service url is " + serviceUrl);
		Doctor[] doctorDetails = restTemplate.getForObject(serviceUrl + "/GetByDoctorId/{doctorId}", Doctor[].class,
				doctorId);
		return Arrays.asList(doctorDetails);
	}
}
