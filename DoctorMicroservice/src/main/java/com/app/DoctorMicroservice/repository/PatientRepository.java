package com.app.DoctorMicroservice.repository;

import java.util.List;
import com.app.DoctorMicroservice.model.Patient;

public interface PatientRepository 
{
	public List<Patient> getDoctorFunction(int patientId, int diagnosisId, int doctorId);

	public List<Patient> findByPatientId(int patientId);
}
