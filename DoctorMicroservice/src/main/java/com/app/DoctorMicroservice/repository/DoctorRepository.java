package com.app.DoctorMicroservice.repository;

import java.util.List;

import com.app.DoctorMicroservice.model.Doctor;

public interface DoctorRepository
{
	public List<Doctor> findByDoctorId(int doctorId);
}
