package com.app.DoctorMicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import com.app.DoctorMicroservice.repository.DoctorRepository;
import com.app.DoctorMicroservice.repository.PatientRepository;
import com.app.DoctorMicroservice.repository.RemoteDoctorRepository;
import com.app.DoctorMicroservice.repository.RemotePatientRepository;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableEurekaClient
//EurekaClient
public class DoctorMicroserviceApplication 
{
	//Service URL
	public static final String
	Patient_SERVICE_URL = "http://Patient-MicroService";

	public static void main(String[] args) 
	{
		SpringApplication.run(DoctorMicroserviceApplication.class, args);
	}
	
	//Swagger
	@Bean
	public Docket DoctorApi()
	{
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.app.DoctorMicroservice")).build();
	}
	
	//RestTemplate
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate()
	{
		return new RestTemplate();
	}
	
	//Object of bean
	@Bean(name="b1")	
	public DoctorRepository doctorRepository()
	{
		return new RemoteDoctorRepository(Patient_SERVICE_URL);
	}
	
	@Bean(name="b2")	
	public PatientRepository patientRepository()
	{
		return new RemotePatientRepository(Patient_SERVICE_URL);
	}

}
