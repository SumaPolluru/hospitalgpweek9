<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">
</head>
<body>

	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/home" class="navbar-brand"><font color=white size=4px>Diagnosis-Page</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/diagnosisTest"><font color=white size=4px>Get
								By DiagnosisID</font></a></li>
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>
	<c:choose>

		<c:when test="${diagnosisobj=='DIAGNOSIS_HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h2>
						<font size=8px>Welcome to Diagnosis MicroService</font>
					</h2>
					<h3>Diagnosis Operations</h3>
				</div>
			</div>
		</c:when>
		<c:when test="${obj=='DIAGNOSIS-DATA' }">
			<div class="container text-center">
				<h3>Enter Diagnosis Id</h3>
				<hr>
				<form class="form-horizontal" action="/getByIdDiagnosis">
					<div class="form-group">
						<label class="control-label col-md-3">Diagnosis Id</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="diagnosisId"
								value="${diagnosis.diagnosisId}" placeholder="diagnosisId"
								required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Submit" />
					</div>
				</form>
			</div>
		</c:when>
	</c:choose>
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>