package com.app.DiagnosisMicroservice.repository;

import java.util.List;

import com.app.DiagnosisMicroservice.model.Diagnosis;

public interface DiagnosisRepository 
{
	public List<Diagnosis> findByDiagnosisId(int diagnosisId);
}