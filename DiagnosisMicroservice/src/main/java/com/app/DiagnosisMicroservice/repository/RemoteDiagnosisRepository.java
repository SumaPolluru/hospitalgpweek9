package com.app.DiagnosisMicroservice.repository;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.app.DiagnosisMicroservice.model.Diagnosis;

@Component
public class RemoteDiagnosisRepository implements DiagnosisRepository 
{

	@Autowired
	protected RestTemplate restTemplate;

	protected String serviceUrl;

	public RemoteDiagnosisRepository() 
	{

	}

	//Passing Service URL
	public RemoteDiagnosisRepository(String serviceUrl) 
	{
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
	}

	//Method to retrieve based on DiagnosisId, passing to service URL
	@Override
	public List<Diagnosis> findByDiagnosisId(int diagnosisId)
	{
		System.out.println("Service url is " + serviceUrl);
		Diagnosis[] diagnosisDetails = restTemplate.getForObject(serviceUrl + "/Diagnosis/{diagnosisId}",
				Diagnosis[].class, diagnosisId);
		return Arrays.asList(diagnosisDetails);
	}
}