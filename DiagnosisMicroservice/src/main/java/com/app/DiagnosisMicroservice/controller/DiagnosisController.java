package com.app.DiagnosisMicroservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.app.DiagnosisMicroservice.model.Diagnosis;
import com.app.DiagnosisMicroservice.repository.DiagnosisRepository;

@RestController
public class DiagnosisController
{

	@Autowired
	@Qualifier("b3")
	DiagnosisRepository diagnosisRepository;

	//Webpage starts
	@RequestMapping("/")
	public ModelAndView welcomepage() 
	{
		System.out.println("Coming here ");
		return new ModelAndView("welcomePage");
	}

	//Home
	@RequestMapping("/home")
	public ModelAndView Welcome(HttpServletRequest request) 
	{
		request.setAttribute("diagnosisobj", "DIAGNOSIS_HOME");
		return new ModelAndView("diagnosisForm");
	}

	//Diagnosis Form to take input(diagnosisId)
	@RequestMapping("/diagnosisTest")
	public ModelAndView functionalTest(HttpServletRequest request)
	{
		request.setAttribute("obj", "DIAGNOSIS-DATA");
		return new ModelAndView("diagnosisForm");
	}

	//Get by DignosisId
	@GetMapping("/getByIdDiagnosis")
	public List<Diagnosis> getByName(@RequestParam int diagnosisId)
	{
		List<Diagnosis> diagnosisData = diagnosisRepository.findByDiagnosisId(diagnosisId);
		return diagnosisData;

	}

	// Logout
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request)
	{
		HttpSession httpSession = request.getSession();
		httpSession.invalidate();
		return new ModelAndView("redirect:/");
	}
}
