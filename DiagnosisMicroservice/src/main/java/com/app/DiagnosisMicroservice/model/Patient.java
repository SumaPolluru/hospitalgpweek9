package com.app.DiagnosisMicroservice.model;

//Patient class for storing the data of Patient table
public class Patient 
{
	private int patientId;
	private String patientName;
	private String patientEmail;
	private int doctorId;
	private int diagnosisId;

	public int getDoctorId() 
	{
		return doctorId;
	}

	public void setDoctorId(int doctorId) 
	{
		this.doctorId = doctorId;
	}

	public int getDiagnosisId() 
	{
		return diagnosisId;
	}

	public void setDiagnosisId(int diagnosisId)
	{
		this.diagnosisId = diagnosisId;
	}

	public int getPatientId()
	{
		return patientId;
	}

	public void setPatientId(int patientId)
	{
		this.patientId = patientId;
	}

	public String getPatientName() 
	{
		return patientName;
	}

	public void setPatientName(String patientName)
	{
		this.patientName = patientName;
	}

	public String getPatientEmail() 
	{
		return patientEmail;
	}

	public void setPatientEmail(String patientEmail) 
	{
		this.patientEmail = patientEmail;
	}

}
