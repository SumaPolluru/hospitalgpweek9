package com.app.DiagnosisMicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import com.app.DiagnosisMicroservice.repository.DiagnosisRepository;
import com.app.DiagnosisMicroservice.repository.RemoteDiagnosisRepository;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableEurekaClient
public class DiagnosisMicroserviceApplication
{

	//Service URL
	public static final String Diagnosis_SERVICE_URL = "http://Patient-MicroService";

	public static void main(String[] args) 
	{
		SpringApplication.run(DiagnosisMicroserviceApplication.class, args);
	}

	//Swagger
	@Bean
	public Docket DiagnosisApi() 
	{
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.app.DiagnosisMicroservice")).build();
	}

	//RestTemplate
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() 
	{
		return new RestTemplate();
	}

	//Object of Bean
	@Bean(name = "b3")
	public DiagnosisRepository diagnosisRepository() 
	{
		return new RemoteDiagnosisRepository(Diagnosis_SERVICE_URL);
	}
}
