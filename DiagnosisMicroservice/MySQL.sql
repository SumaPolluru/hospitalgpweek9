
mysql> create database microservicedb;
Query OK, 1 row affected (0.01 sec)

mysql> use microservicedb;
Database changed

mysql> show tables;
+--------------------------+
| Tables_in_microservicedb |
+--------------------------+
| diagnosis                |
| doctor                   |
| patient                  |
+--------------------------+
3 rows in set (0.00 sec)

mysql> show tables;
+--------------------------+
| Tables_in_microservicedb |
+--------------------------+
| diagnosis                |
| doctor                   |
| patient                  |
+--------------------------+
3 rows in set (0.00 sec)

insert into diagnosis values(1,"Influenza");

insert into diagnosis values(2,"smallpox");

insert into diagnosis values(3,"cold");

mysql> insert into diagnosis values(1,"Influenza");
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> insert into diagnosis values(2,"smallpox");
Query OK, 1 row affected (0.00 sec)

mysql>
mysql> insert into diagnosis values(3,"cold");;
Query OK, 1 row affected (0.00 sec)

mysql> insert into diagnosis values(4,"HeartAttack");
Query OK, 1 row affected (0.01 sec)

mysql> insert into diagnosis values(5,"Kidney failure");
Query OK, 1 row affected (0.01 sec)

mysql> select * from diagnosis;
+--------------+----------------+
| diagnosis_id | disease_name   |
+--------------+----------------+
|            1 | Influenza      |
|            2 | smallpox       |
|            3 | cold           |
|            4 | HeartAttack    |
|            5 | Kidney failure |
+--------------+----------------+
5 rows in set (0.00 sec)

mysql> desc doctor;
+----------------+--------------+------+-----+---------+-------+
| Field          | Type         | Null | Key | Default | Extra |
+----------------+--------------+------+-----+---------+-------+
| doctor_id      | int          | NO   | PRI | NULL    |       |
| doctor_name    | varchar(255) | YES  |     | NULL    |       |
| specialization | varchar(255) | YES  |     | NULL    |       |
+----------------+--------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

insert into doctor values(4,"Dr.Suma"," Physician");

insert into doctor values(2,"Dr.Ram"," Cardilogist");

insert into doctor values(1,"Dr.Sree","Gynacologist");

insert into doctor values(3,"Dr.Gayu","dermathology");

mysql> desc patient;
+---------------+--------------+------+-----+---------+-------+
| Field         | Type         | Null | Key | Default | Extra |
+---------------+--------------+------+-----+---------+-------+
| patient_id    | int          | NO   | PRI | NULL    |       |
| diagnosis_id  | int          | NO   | MUL | NULL    |       |
| doctor_id     | int          | NO   | MUL | NULL    |       |
| patient_email | varchar(255) | YES  |     | NULL    |       |
| patient_name  | varchar(255) | YES  |     | NULL    |       |
+---------------+--------------+------+-----+---------+-------+
5 rows in set (0.00 sec)

 insert into patient values(1,1,1,"Vasu@gmail.com","vamsi");

 insert into patient values(2,3,4,"krishna@gmail.com","kushi");
 insert into patient values(3,3,4,"Sree@gmail.com","Sree");

 insert into patient values(4,2,4,"Riya@gmail.com","Riya");

mysql>
mysql> select * from patient;
+------------+--------------+-----------+-------------------+--------------+
| patient_id | diagnosis_id | doctor_id | patient_email     | patient_name |
+------------+--------------+-----------+-------------------+--------------+
|          2 |            3 |         4 | krishna@gmail.com | kushi        |
|          1 |            1 |         1 | Vasu@gmail.com    | vamsi        |
|          3 |            3 |         4 | Sree@gmail.com    | Sree         |
|          4 |            2 |         4 | Riya@gmail.com    | Riya         |
+------------+--------------+-----------+-------------------+--------------+
4 rows in set (0.00 sec)
